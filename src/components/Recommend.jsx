import React from "react"
import styled from "styled-components"
import { StaticImage, GatsbyImage } from "gatsby-plugin-image"

export default function Recommend({ recomendaciones }) {
  return (
    <Section id="recommend">
      <div className="title">
        <h2>Recommended Destinations</h2>
      </div>
      <div className="destinations">
        {recomendaciones.map((destination, index) => {
          return (
            <div className="destination" key={index}>
              <GatsbyImage image={destination.imagen} alt={destination.slug} />
              <h3>{destination.titulo}</h3>
              <p>{destination.descripcion}</p>
              <div className="info">
                <div className="services">
                  <StaticImage src={"../images/info1.png"} alt="info1" />
                  <StaticImage src={"../images/info2.png"} alt="info2" />
                  <StaticImage src={"../images/info3.png"} alt="info3" />
                </div>
                <h4>{destination.costo}</h4>
              </div>
              <div className="distance">
                <span>{destination.distancia}</span>
                <span>{destination.estadia}</span>
              </div>
            </div>
          )
        })}
      </div>
    </Section>
  )
}

const Section = styled.section`
  padding: 2rem 0;
  .title {
    text-align: center;
  }
  .destinations {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    gap: 3rem;
    padding: 0 3rem;
    .destination {
      padding: 1rem;
      display: flex;
      flex-direction: column;
      gap: 0.5rem;
      background-color: #8338ec14;
      border-radius: 1rem;
      transition: 0.3s ease-in-out;
      &:hover {
        transform: translateX(0.4rem) translateY(-1rem);
        box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
      }
      .gatsby-image-wrapper{
        div:first-child{
          max-width: 100% !important;
        }
      }
      img {
        width: 100%;
      }
      .info {
        display: flex;
        align-items: center;
        .services {
          display: flex;
          gap: 0.3rem;
          img {
            border-radius: 1rem;
            background-color: #4d2ddb84;
            width: 2rem;
            padding: 0.3rem 0.4rem;
          }
        }
        display: flex;
        justify-content: space-between;
      }
      .distance {
        display: flex;
        justify-content: space-between;
      }
    }
  }
  @media screen and (min-width: 280px) and (max-width: 768px) {
    .destinations {
      grid-template-columns: 1fr;
      padding: 0;
    }
  }
`
