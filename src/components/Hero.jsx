import React from "react"
import { GatsbyImage } from "gatsby-plugin-image"
import styled from "styled-components"

import { useStaticQuery, graphql } from "gatsby"

export default function Hero() {
  const { allDatoCmsHero } = useStaticQuery(graphql`
    query {
      allDatoCmsHero {
        nodes {
          titulo
          descripcion
          imagen {
            gatsbyImageData
          }
        }
      }
    }
  `)

  const {
    titulo,
    descripcion,
    imagen: { gatsbyImageData },
  } = allDatoCmsHero.nodes[0]

  return (
    <Section id="hero">
      <div className="background">
        <GatsbyImage image={gatsbyImageData} alt={"Hero"} />
      </div>
      <div className="content">
        <div className="title">
          <h1>{titulo}</h1>
          <p>{descripcion}</p>
        </div>
      </div>
    </Section>
  )
}

const Section = styled.section`
  position: relative;
  margin-top: 2rem;
  width: 100%;
  height: 100%;

  .background {
    height: 100%;
    img {
      width: 100%;
      filter: brightness(60%);
    }
  }
  .content {
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0;
    z-index: 3;
    text-align: center;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    gap: 1rem;
    .title {
      color: white;
      h1 {
        font-size: 3rem;
        letter-spacing: 0.2rem;
      }
      p {
        text-align: center;
        padding: 0 30vw;
        margin-top: 0.5rem;
        font-size: 1.2rem;
      }
    }
  }
  @media screen and (min-width: 280px) and (max-width: 980px) {
    height: 25rem;
    .background {
      background-color: palegreen;
      img {
        height: 100%;
      }
    }
    .content {
      .title {
        h1 {
          font-size: 1rem;
        }
        p {
          font-size: 0.8rem;
          padding: 1vw;
        }
      }
    }
  }
`
