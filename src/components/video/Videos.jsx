import React from 'react';

export default function Videos(url){
   
    return (
        <>


            <div className="row justify-content-center">
                <div className="col-8 px-0 marco__back_video d-flex justify-content-center align-items-center">
                    <div className="video_container">
                        <iframe src={url.url} 
                        frameBorder="0"
                        allow="autoplay,
                        allowfullscreen"
                        className="video_full"
                        title="video">
                    </iframe>
                    </div>
                </div>
            </div>
        </>
    );   
}