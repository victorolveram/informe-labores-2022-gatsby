import * as React from "react"
import 'bootstrap/dist/css/bootstrap.min.css';
import '../assets/css/style.css';
import '../assets/css/style_responsive.css';
import Header from "../pages/layout/Header";
import Footer from "../pages/layout/Footer";

const Layout = ({ children }) => {
 
  return (
  <>
    <header>
      <Header/>
    </header> 
    <main className="principal_container">
      {children}
    </main> 
    <footer>      
      <Footer/>
    </footer>
  </>
  )
}

export default Layout
