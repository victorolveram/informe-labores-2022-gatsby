
$(window).scroll(function(){

    
    
    if ( $(window).scrollTop() >= 220 ) {
        
        
        
        $(".nav-item").css("background-color", "#24135F");
        $(".nav-link").css("background-color", "#24135F");
        $(".navbar-nav").css("background-color", "#24135F");
        $(".navbarMenuMain").css("background-color", "#24135F");
        $(".nav-link").css("color", "#fff");
        $(".dropdown-item").css("color", "#fff");
        
        $(".logoHeader").addClass( "reducido" ) ;
        
        $(".nav-link").mouseover(function() {
            $(this).css("color", "#2c2c3a");
            $(this).css("background-color","#FFF");
        }).mouseout(function(){
            $(this).css("background-color","#24135F");
            $(this).css("color", "#FFF");
        });
        
        $(".dropdown-item").mouseover(function() {
            $(this).css("color", "#2c2c3a");
            $(this).css("background-color","#FFF");
        }).mouseout(function(){
            $(this).css("background-color","#24135F");
            $(this).css("color", "#FFF");
        });
        

    } else {
        
        
        $(".navbarMenuMain").css("background-color", "#fff");
        $("#navbarDropdown").css("background-color", "#fff");
        $(".navbar-nav").css("background-color", "#fff");
        $(".nav-item").css("background-color", "#fff");
        $(".nav-link").css("background-color", "#fff");
        $(".nav-link").css("color", "#2c2c3a");
        $(".dropdown-item").css("color", "#2c2c3a");
        
        $(".logoHeader").removeClass( "reducido" ) ;
        
        $(".nav-link").mouseover(function() {
            $(this).css("color", "#FFF");
            $(this).css("background-color","#24135F");
        }).mouseout(function(){
            $(this).css("background-color","#FFF");
            $(this).css("color", "#2c2c3a");
        });
        
        $(".dropdown-item").mouseover(function() {
            $(this).css("color", "#FFF");
            $(this).css("background-color","#24135F");
        }).mouseout(function(){
            $(this).css("background-color","#FFF");
            $(this).css("color", "#2c2c3a");
        });
        
    }


});


$(document).ready(function () {

    $('.up-container').click(function () {
        $('body, html').animate({
            scrollTop: 0
        }, 1050);
    });
});

