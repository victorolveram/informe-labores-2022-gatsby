export const base_url = process.env.REACT_APP_BASE_URL;
export const base_url_completa = process.env.REACT_APP_BASE_URL_COMPLETA;
export const base_pdf = process.env.REACT_APP_PATH_PDF;