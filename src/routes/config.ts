
import Redireccionar from "../utilities/Redirect/Redirect404"
import { base_url } from "../constants";
import Home from "../pages/home/index";
import Capitulos from "../pages/capitulos/main";
import Datos from "../pages/datos/main";
import Magistrados from "../pages/magistrados/main";
import Demo from "../pages/prueba/Demo";

const rutas = [     
    { path: `/${base_url}/holi`, component: Demo },
    { path: `/${base_url}/capitulos/:id`, component: Capitulos  },
    { path: `/${base_url}/magistrados/:id`, component: Magistrados },
    { path: `/${base_url}/datos`, component: Datos  },
    { path: `/${base_url}/`, component: Home, exact: true },
    { path: '*', component: Redireccionar },
]; 

export default rutas;