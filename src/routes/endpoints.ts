const apiUrl = process.env.REACT_APP_API_URL;

export const webApiInforme= `${apiUrl}/Servicio/informe`

export const webApiCapitulos= `${apiUrl}/Servicio/capitulos`
export const webApiMenu= `${apiUrl}/Servicio/menu`
export const webApiNumeroWhats= `${apiUrl}/Servicio/numero_whats` 
export const webApiMagistrados= `${apiUrl}/Servicio/magistrados`
export const webApiMagistradosDetalle= `${apiUrl}/Servicio/magistradosDetalle`
export const webApiVideoteca= `${apiUrl}/Servicio/videoteca`
export const webApiVideotecaDetalle= `${apiUrl}/Servicio/videotecaDetalle`
export const webApiVideos= `${apiUrl}/Servicio/videos`

 
export const webApiDatosTemas = `${apiUrl}/DatosAbiertos/temas`
export const webApiDatosBusqueda = `${apiUrl}/DatosAbiertos/busqueda`

export const pathImages = process.env.REACT_APP_PATH_IMAGES;
export const pathPDF = process.env.REACT_APP_PATH_PDF;
export const nombreSitio = process.env.REACT_APP_SITIO_NOMBRE;
