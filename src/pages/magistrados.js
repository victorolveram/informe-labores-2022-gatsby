import * as React from "react"
import Layout from "../components/layout"
import Seo from "../components/seo"
import Magistrado from "./home/partials/Magistradas"

const Magistrados = () => (
  <Layout>
    <Magistrado tipo={2}/>
  </Layout>
)

export const Head = () => <Seo title="Magistrados" />

export default Magistrados
