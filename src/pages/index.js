import * as React from "react"
import Layout from "../components/layout"
import FirstSection from "./home/partials/FirstSection"
import Magistradas from "./home/partials/Magistradas"
import Comercial from "./home/partials/Comercial"
import Capitulos from "./home/partials/Capitulos"
import Videoteca from "./home/partials/Videoteca"
import Unete from "./home/partials/Unete" 

const Index2Page = () => {
  return (
    <Layout>
      <FirstSection/>
      <Magistradas tipo={1}/>
      <Comercial/>
      <Capitulos/>      
      <Videoteca/>
      <Unete/>
    </Layout> 
  ) 
}

export default Index2Page
