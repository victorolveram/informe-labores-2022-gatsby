export interface informeDTO {
    descripcion:string,
    pdf: string,
    url: string,
    video: string
}
export interface capituloDTO {
    titulo:string,
    tipo:number,
    video:string,
    descripcion:string,
    pdf:string
}
export interface magistradoDTO {
    id:number,
    imagen:string,
    nombre:string,
    paterno:string,
    materno:string,
    descripcion:string,
    orden:string,
    perfil:string

}
export interface videotecaDTO {
    id:number
    imagen:string,
    url:string,
    tipoVideo:number,
    video:string
}
export interface comercialDTO {
    video:string
}