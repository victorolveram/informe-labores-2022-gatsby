import React from 'react';
import play_pink from '../../../assets/images/svg/play_pink.svg';
import arrow_down_dos from '../../../assets/images/svg/arrow_down_dos.svg';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { webApiVideos } from '../../../routes/endpoints';

const PlayCollapse = ({informacion}) => {    
    const [videoInicial,setVideoInicial]=useState();  
    // const informacion2 = [{descripcion:'des'}]
    var descripcion = '';
    if(informacion !== undefined){
        descripcion = informacion[0];
        descripcion = descripcion.descripcion;
    }
   // const {descripcion} = Array.isArray(informacion)? informacion[0]:[{descripcion:'des'}]; 
    //console.log(descripcion); 
    useEffect(()=>{
        axios.post(webApiVideos,{id:1}).then((response) => {
            const video = (response.data[0].video!==null)?response.data[0].video:''
            setVideoInicial(video);
        }).catch(err => console.warn(err)); 
    },[])
    return(
        <>
        <div className="container-fluid play_collapse">
            <div className="row">
                <div className="col-sm-4 d-flex align-items-center play_zone">
                    <a href={videoInicial} target="_blank" rel="noreferrer">
                    <span className="cursor_hand">    
                        <img src={play_pink} alt="play icono" className="play_pink"/>
                    </span>
                    </a> 
                    <div className="ms-3 my-3 my-sm-0">
                        <h4 className="mb-1">#ENVIVO</h4>
                        <p className="mb-0">Presentación del Informe de Labores 2021-2022 ante la Suprema Corte de lusticia de la Nación.</p>
                    </div> 
                </div>
                <div className="col-sm-8 p-0">
                    <button type="button" className="btn collapse_play_btn" data-bs-toggle="collapse" data-bs-target="#demo">
                        <div className="col-sm-4w-100 d-flex justify-content-end">
                            <img src={arrow_down_dos} alt="colapsable abrir" className="arrow_down_icon"/>    
                        </div>                        
                    </button>
                </div>
                <div id="demo" className="collapse col-sm-12 mt-2">
                    <div className="row justify-content-center">
                        <div className="col-md-11 p-5">
                            <p className="mb-0" dangerouslySetInnerHTML={{__html: `${descripcion}` }}></p> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>

    )
};
export default PlayCollapse;