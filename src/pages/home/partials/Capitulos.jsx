import React from 'react'
import axios from 'axios'
import { useEffect, useState } from 'react'
import { Link } from 'gatsby'
import light_book from '../../../assets/images/svg/light_book.svg'
import digital_square from '../../../assets/images/svg/digital_square.svg'
import puzzle_light from '../../../assets/images/svg/puzzle_light.svg'
import connection_people from '../../../assets/images/svg/connection_people.svg'
import light_search from '../../../assets/images/svg/light_search.svg'
import objetive_arrow from '../../../assets/images/svg/objetive_arrow.svg'
import { webApiCapitulos} from '../../../routes/endpoints';

const Capitulos = () =>{
    const [introCapitulo,setIntroCapitulo] = useState();
    useEffect(()=>{
        axios.post(webApiCapitulos,{tipo:8}).then((response) => {
            setIntroCapitulo(response.data);
        }).catch(err => console.warn(err)); 
         
    },[])
    if(!introCapitulo)
    {
        return(  
             <div>CARGANDO....</div>   
        )    
    }else{  
       const {descripcion} = introCapitulo[0];       
        return(
            <>
                <div className="ontainer-fluid mt-5">
                    <div className="row  justify-content-center">
                        <div className="col-12 title_section text-center">
                            <h2 className="mb-0 d-block ">CONTENIDO</h2>
                        </div>
                        <div className="col-11">
                            <p className="paragraph_content  pb-3" dangerouslySetInnerHTML={{__html: `${descripcion}` }}></p>
                        </div>
                    </div>
                </div>

                <div className="container-fluid capitulos_container">
                    <div className="row justify-content-center">
                        <div className="col-sm-10 capitulos_grid my-4">
                            <div className="row">
                                <div className="col-sm-6 col-md-4">
                                    <Link to="capitulos?q=1" className="chapter_one d-flex align-items-center justify-content-center">
                                        <h2 className="text-white text-center px-5 capitulo_title mb-0">innovación y buenas prácticas</h2>
                                        <img src={light_book} alt="icono de inovación y buenas practicas" className="btn_icon_captiulos" />
                                    </Link>
                                </div>
                                <div className="col-sm-6 col-md-4 mt-3 mt-sm-0">
                                    <Link to="capitulos?q=2" className="chapter_one d-flex align-items-center justify-content-center">
                                        <h2 className="text-white text-center px-5 capitulo_title mb-0">abierto, digital y colaborativo</h2>
                                        <img src={digital_square} alt="icono de inovación y buenas practicas" className="btn_icon_captiulos" />
                                    </Link>
                                </div>
                                <div className="col-sm-6 col-md-4 mt-3 mt-md-0">
                                    <Link to="capitulos?q=3" className="chapter_one d-flex align-items-center justify-content-center">
                                        <h2 className="text-white text-center px-5 capitulo_title mb-0">independiente y confiable</h2>
                                        <img src={connection_people} alt="icono de inovación y buenas practicas" className="btn_icon_captiulos" />
                                    </Link>
                                </div>
                                <div className="col-sm-6 col-md-4 mt-3">
                                    <Link to="capitulos?q=4" className="chapter_one d-flex align-items-center justify-content-center">
                                        <h2 className="text-white text-center px-5 capitulo_title mb-0">incluyente y de derechos</h2>
                                        <img src={puzzle_light} alt="icono de inovación y buenas practicas" className="btn_icon_captiulos" />
                                    </Link>
                                </div>
                                <div className="col-sm-6 col-md-4 mt-3">
                                    <Link to="capitulos?q=5" className="chapter_one d-flex align-items-center justify-content-center">
                                        <h2 className="text-white text-center px-5 capitulo_title mb-0">íntegro y profesional</h2>
                                        <img src={light_search} alt="icono de inovación y buenas practicas" className="btn_icon_captiulos" />
                                    </Link>
                                </div>
                                <div className="col-sm-6 col-md-4 mt-3">
                                    <Link to="capitulos?q=6" className="chapter_one d-flex align-items-center justify-content-center">
                                        <h2 className="text-white text-center px-5 capitulo_title mb-0">eficiente y eficaz</h2>
                                        <img src={objetive_arrow} alt="icono de inovación y buenas practicas" className="btn_icon_captiulos" />
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
            </>    
        )   
    }
}
export default Capitulos;