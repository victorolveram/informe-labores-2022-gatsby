import React from 'react';
import tablet from '../../../assets/images/tablet.png';
import whatsapp_unete from '../../../assets/images/svg/whatsapp_unete.svg';
import { useEffect, useState } from 'react';
import { webApiNumeroWhats } from '../../../routes/endpoints';
import axios from 'axios';


const Unete = ()=>{
    const [numero, setNumero] = useState('')
    useEffect(()=>{
        axios.get(webApiNumeroWhats).then((response) => {               
            setNumero(`https://wa.me/${response.data[0].numero}`);            
        }).catch(err => console.warn(err));         
    },[])
    return(
        <>
            <div className="container-fluid">
                <div className="row">
                    
                        <div className="col-12 unete_container">
                        <a href={numero} target="blank">
                            <img src={whatsapp_unete} alt="whatsapp unete" className="whats_unete"/>
                            <div className="row h-md-100 h-100">
                                <div className="col-4 d-flex align-items-end justify-content-end d-none d-sm-block">
                                    <div className="d-flex align-items-end justify-content-end h-100">
                                        <img src={tablet} alt="tableta" className="tableta_img"/>
                                    </div>
                                </div>
                                <div className="col-sm-8 mt-4 mt-md-0 d-md-flex align-items-center">
                                        <div className="unete_text">
                                            <h3 className="unete_title">únete a la comunidad tepjf</h3>
                                            <div className="row">
                                                <div className="col-md-5">
                                                    <p>
                                                        Forma parte de nuestra lista de contactos preferidos y recibe información del Tribunal antes que los demás.
                                                    </p>
                                                    <p>
                                                        Envíanos tu información de whatsapp al siguiente correo: prensa@te.gob.mx y te haremos llegar información diaria.     
                                                    </p>
                                                </div>
                                                <div className="col-md-6">
                                                    <p className="italic_text">RECUERDA QUE LA INFORMACIÓN QUE MANDAS AL CORREO ES COMPLETAMENTE CONFIDENCIAL</p>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            </a>
                        </div>
                    
                </div>
                <div className="row d-flex justify-content-center align-item-center">
                    <div className="col-12 title_section">
                    </div>
                </div>
            </div>
        </>
    )
}
export default Unete;