import React from 'react';
import Videos from "../../../components/video/Videos";
import { useEffect,useState } from "react";
import axios from "axios";
import { webApiVideos } from "../../../routes/endpoints";

const Comercial = () => {
    const [comercial,setComercial] = useState();
    useEffect(()=>{
        axios.post(webApiVideos,{id:2}).then((response) => {
            setComercial(response.data);
        }).catch(err => console.warn(err));          
    },[])
    if(!comercial){
        return(
            <><h1>Cargando....</h1></>
        )
    }else{
        const {video}=comercial[0]
        return(
            <>
                <div className="container-fluid">
                    <Videos url={video}/>
                </div>
            </>
        );
    }
    

    
}
export default Comercial