import React from 'react';
import { pathPDF } from "../../../routes/endpoints";
import demo from '../../../assets/images/svg/informe_logo.svg'
import arrow_down from '../../../assets/images/svg/arrow_down.svg'
import greca_left from '../../../assets/images/greca_left.png'
import greca_right from '../../../assets/images/greca_right.png'
import arrow_right from '../../../assets/images/svg/arrow_right.svg'
import * as stylesFirstSection from '../index.module.css'
import PlayCollapse from './PlayCollapse';
import { webApiInforme } from '../../../routes/endpoints';
import axios from 'axios';
import { useEffect, useState } from 'react';


const FirstSection = () => {  
    const [informe, setInforme] = useState();
    useEffect(() => {
        try {
            axios.get(webApiInforme)
            .then(respuesta =>{
              setInforme(respuesta.data)
            })        
          } catch (error) {
            console.log("el error es", error);
          }
      }, []);
if(!informe){
    return(  
        <div>CARGANDO....</div>   
    )    
}else{  
    return(
        <>
        <div className="container-fluid first_section">
            <div className="row first_container">
                <div className={`col-12 ${stylesFirstSection.logo_container}`}> 
                    <div className="row">
                        <div className="col-sm-5 mt-5 text-center text-sm-start">
                            <img src={demo} alt="informe de labores 2021/2022" className={`ms-sm-5 ${stylesFirstSection.informe_logo} img-fluid`}/>
                        </div>
                        <div className="col-sm-7 mt-4 ms-sm-5">
                            <div className={`row ${stylesFirstSection.descarga_bloc}`}>
                                <div className="col-2 d-none d-sm-flex"></div>
                                <div className="col-sm-10 d-flex align-items-center">
                                    <a href={`${pathPDF}${informe[0].pdf}`} className={`${stylesFirstSection.link_descarga} my-3 my-sm-0`} rel="noreferrer" target="_blank">
                                        <div className="row">
                                            <div className="col-11 pe-0">
                                                <h4 className="">DESCARGA EL ARCHIVO COMPLETO</h4>
                                                <div className="text-white d-flex align-items-center justify-content-between">
                                                    <h6 className="mb-0 me-2">EN EL SIGUIENTE ENLACE</h6>
                                                    <div className={`img-fluid ${stylesFirstSection.pleca_descarga}`}></div>
                                                </div>
                                            </div><div className="col-1 d-flex align-items-center justify-content-center">
                                                <img src={arrow_down} alt="" className="arrow_down"/>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={`col-6 col-md-4 col-2 ps-0 ${stylesFirstSection.greca_left} h-100`}>
                    <img src={greca_left} alt="grecas" className={`img-fluid ${stylesFirstSection.greca_img}`}/>
                                
                </div>
                <div className={`col-6 col-md-4 col-2 ps-0 ${stylesFirstSection.greca_right} h-100 px-0`}> 
                    <img src={greca_right} alt="grecas" className={`${stylesFirstSection.greca_img} float-end`}/>
                </div>
                <div className="d-none d-sm-block col-5">
                </div>
                <div className="col-sm-7 px-0">
                    <div className={`${stylesFirstSection.bg_photo_libertad} vh-100`}>
                        <div className="d-flex justify-content-end w-100"> <a href="/#">
                            <img src={arrow_right} alt="flecha derecha" className={`${stylesFirstSection.arrow_right} d-none d-sm-block`}/></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <PlayCollapse informacion={informe}/>
        </>
    )
}
};
export default FirstSection;
