import React from 'react';
import { useEffect, useState } from "react";
import axios from "axios";
import Videos from "../../../components/video/Videos";
import { webApiVideotecaDetalle } from "../../../routes/endpoints";
const VideotecaDetalle = ({id})=>{
    const [videotecaDetalle,setVideotecaDetalle]=useState()
    useEffect(()=>{
        axios.post(webApiVideotecaDetalle,{id:id}).then((response) => {
            setVideotecaDetalle(response.data);
        }).catch(err => console.warn(err)); 
    },[id])
    
    if(videotecaDetalle){
        const {video}=videotecaDetalle[0]        
        return(        
            <div className="container-fluid videoteca_container mb-4">
                <div className='row'> 
                    <Videos url={video}/>
                </div>
            </div>
            )
    }else{
        return(
            <><h1>Cargando...</h1></>
        )
    }
}
export default VideotecaDetalle;