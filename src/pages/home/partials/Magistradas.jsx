import React from 'react';
import axios from "axios";
import { useEffect, useState } from 'react';
import { Link } from "gatsby"
import { webApiMagistrados,pathImages} from '../../../routes/endpoints';
 import Biografia from "../../magistrados/partials/biografia";
 import {useLocation} from '@reach/router'

const Magistradas = (tipo) => { 
    const [idMagistrado, setIdMagistrado] = useState(1)
    const [Magistrado,setMagistrado] = useState();    
    const location = useLocation()
    const search = new URLSearchParams(location.search)   
    const id = search.get('q')
    useEffect(()=>{
        window.scrollTo(0, 0)
        setIdMagistrado(id)
        axios.get(webApiMagistrados).then((response) => {
            setMagistrado(response.data);            
        }).catch(err => console.warn(err));  
    },[id])
    const getMagistrado=(id)=>{        
        setIdMagistrado(id)
        window.scrollTo(0, 300)        
    }
    return(
        <>
        <div className="container">
            <div className="row">
                <div className="col-12 title_section text-center">
                    <h2 className="mb-0 d-block">Magistraturas</h2>
                </div>
                <div className="col-12 mb-4">
                    <div className="row justify-content-center d-flex">
                        {
                            Magistrado?.map((value,i)=>  
                                <div className={(value.id > 1)?`col-6 col-sm-auto mb-3 d-flex d-flex justify-content-center order-2 order-md-0`:`col-sm-auto mb-3 d-flex d-flex justify-content-center order-1 order-md-0`} key={i}>
                                    <div className="text-center profile_container ">
                                        <div className="container_profile mx-auto">
                                            <img src={`${pathImages}${value.imagen}`} alt="Reyes Rodríguez Mondragón"
                                                className="image_profile"/>                                            
                                               { (tipo.tipo===1)
                                                ?<Link to={`magistrados?q=${value.id}`} className="overlay_profile d-flex align-items-center text-center">
                                                    <h5 className="mb-0 p-2">{value.perfil}</h5>
                                                </Link> 
                                                :
                                                <div className="overlay_profile d-flex align-items-center text-center cursor_hand" role="presentation" onClick={(e)=>getMagistrado(value.id)}>
                                                    <h5 className="mb-0 p-2">{value.perfil}</h5>     
                                                </div>                                         
                                               }                                            
                                        </div>
                                            <p className="mb-0 profile_name mt-2">{value.nombre}</p>
                                            <p className="mb-0 profile_lastname">{`${value.paterno} ${value.materno}`}</p>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                </div>
            </div>
        </div>
        { (tipo.tipo===2) && <Biografia info={idMagistrado} />  }
        </>

    );
}
export default Magistradas