import React from 'react'
import link_icon from '../../../assets/images/svg/link_icon.svg'
import { useState,useEffect } from 'react'
import { pathImages } from '../../../routes/endpoints'
import axios from 'axios'
import lupa_icon from '../../../assets/images/svg/lupa_icon.svg'
import { webApiVideoteca } from '../../../routes/endpoints'
import VideotecaDetalle from './VideotecaDetalle'

const Videoteca = ()=>{
    const [videotecaDetalle,setVideotecaDetalle] = useState(0);
    const [videoteca,setVideoteca] = useState();
    useEffect(()=>{
        axios.get(webApiVideoteca).then((response) => {
            setVideoteca(response.data);
        }).catch(err => console.warn(err)); 
    },[])
    //console.log(videoteca);
    const getVideotecaDetalle=(id)=>{    
        const element = document.getElementById('videoContenedor')
        element.scrollIntoView({ behavior: 'smooth', block: "center" });
        setVideotecaDetalle(id)
    }
    if(videoteca){
        return(
            <>
                <div className="container-fluid videoteca_container">
                    <div className="row d-flex justify-content-center align-item-center">
                        <div className="col-12 title_section">
                            <h2 className="mb-0 d-block">VIDEOTECA</h2>
                        </div>        
                    </div>
                    <div className="row">
                        <div className="grecas_bottom_main px-0 d-flex align-items-end " >   
                        </div>    
                        <div className="col-sm-4 px-0 fotodiv foto_general" style={{backgroundImage: `url(${pathImages}${videoteca[0].imagen})`  }}>
                            <div className="link_foto d-flex justify-content-center">
                                <div className="">
                                    <a href="/#">
                                        <img src={link_icon} alt="link icono" className="me-1"/>                                        
                                    </a>
                                    <span role="presentation" onClick={(e)=>getVideotecaDetalle(videoteca[0].id)}>
                                        <img src={lupa_icon} alt="lupa icono" className="ms-1"/>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm px-0 fotodiv foto_general" style={{backgroundImage: `url(${pathImages}${videoteca[1].imagen})`  }}>
                            <div className="link_foto d-flex justify-content-center">
                                <div className="">
                                    <a href="/#">
                                        <img src={link_icon} alt="link icono" className="me-1"/>
                                    </a>
                                    <span role="presentation" onClick={(e)=>getVideotecaDetalle(videoteca[1].id)}>
                                        <img src={lupa_icon} alt="lupa icono" className="ms-1"/>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm px-0 fotodiv foto_general" style={{backgroundImage: `url(${pathImages}${videoteca[2].imagen})`  }}>
                            <div className="link_foto d-flex justify-content-center">
                                <div className="">
                                    <a href="/#">
                                        <img src={link_icon} alt="link icono" className="me-1"/>
                                    </a>
                                    <span role="presentation" onClick={(e)=>getVideotecaDetalle(videoteca[2].id)}>
                                        <img src={lupa_icon} alt="lupa icono" className="ms-1"/>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm px-0 fotodiv foto_general" style={{backgroundImage: `url(${pathImages}${videoteca[3].imagen})`  }}>
                            <div className="link_foto d-flex justify-content-center">
                                <div className="">
                                    <a href="/#">
                                        <img src={link_icon} alt="link icono" className="me-1"/>
                                    </a>
                                    <span role="presentation" onClick={(e)=>getVideotecaDetalle(videoteca[3].id)}>
                                        <img src={lupa_icon} alt="lupa icono" className="ms-1"/>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm px-0 fotodiv foto_general" style={{backgroundImage: `url(${pathImages}${videoteca[4].imagen})`  }}>
                            <div className="link_foto d-flex justify-content-center">
                                <div className="">
                                    <a href="/#">
                                        <img src={link_icon} alt="link icono" className="me-1"/>
                                    </a>
                                    <span role="presentation" onClick={(e)=>getVideotecaDetalle(videoteca[4].id)}>
                                        <img src={lupa_icon} alt="lupa icono" className="ms-1"/>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm px-0 fotodiv foto_general" style={{backgroundImage: `url(${pathImages}${videoteca[5].imagen})`  }}>
                            <div className="link_foto d-flex justify-content-center">
                                <div className="">
                                    <a href="/#">
                                        <img src={link_icon} alt="link icono" className="me-1"/>
                                    </a>
                                    <span role="presentation" onClick={(e)=>getVideotecaDetalle(videoteca[5].id)}>
                                        <img src={lupa_icon} alt="lupa icono" className="ms-1"/>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm px-0 fotodiv foto_general" style={{backgroundImage: `url(${pathImages}${videoteca[6].imagen})`  }}>
                            <div className="link_foto d-flex justify-content-center">
                                <div className="">
                                    <a href="/#">
                                        <img src={link_icon} alt="link icono" className="me-1"/>
                                    </a>
                                    <span role="presentation" onClick={(e)=>getVideotecaDetalle(videoteca[6].id)}>
                                        <img src={lupa_icon} alt="lupa icono" className="ms-1"/>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-4 px-0 fotodiv foto_general" style={{backgroundImage: `url(${pathImages}${videoteca[7].imagen})`  }}>
                            <div className="link_foto d-flex justify-content-center">
                                <div className="">
                                    <a href="/#">
                                        <img src={link_icon} alt="link icono" className="me-1"/>
                                    </a>
                                    <span role="presentation" onClick={(e)=>getVideotecaDetalle(videoteca[7].id)}>
                                        <img src={lupa_icon} alt="lupa icono" className="ms-1"/>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div className="row d-flex justify-content-center align-item-center">
                            <div className="col-12 title_section">
                            </div>
                        </div>
                    </div>
                </div>
                <div id='videoContenedor'> {(videotecaDetalle!==0)&& <VideotecaDetalle id={videotecaDetalle}/>} </div>
            
        </>
        )
    }
    else{
        return(<></>)
    }
}
export default Videoteca;
