import * as React from "react"
import Layout from "../components/layout"
import Seo from "../components/seo"
import Capitulo from "../pages/capitulos/partials/Capitulos"

const Capitulos = () => (
  <Layout>
    <Capitulo/>
  </Layout>
)

export const Head = () => <Seo title="Capitulos" />

export default Capitulos