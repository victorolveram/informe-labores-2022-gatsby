import React from 'react'
import { useEffect, useState } from 'react';
import axios from "axios";
import { webApiCapitulos } from "../../../routes/endpoints";
import VideoYoutube from "../../../components/video/Videos";
import {useLocation} from '@reach/router'


const Capitulos = ()=>{
    const location = useLocation()
    const search = new URLSearchParams(location.search)   
    const id = search.get('q')
    const [capitulos,setCapitulos] = useState();    
    useEffect(()=>{
        window.scrollTo(0, 0)
        axios.post(webApiCapitulos,{tipo:id}).then((response) => {
            setCapitulos(response.data);
        }).catch(err => console.warn(err)); 
    },[id])
    if(!capitulos){
        return(  
             <div>CARGANDO....</div>   
        )    
    }else{  
        if(capitulos.length){
            const  {descripcion,titulo,video} = capitulos[0];            
            return(
                <>
                    <div className="container-fluid">
                        <div className="row justify-content-center">
                            <div className="col-12 title_section text-center">
                                <h1 className="mb-0 d-block ml-3">{titulo}</h1>                                
                            </div>
                            {(video!=='') &&

                                <div className="col-10 px-0">
                                    
                                    <VideoYoutube url={video}/>
                                </div>
                            } 
                        </div>
                    </div>
                    <div dangerouslySetInnerHTML={{__html: `${descripcion}` }}></div>
                </>
            );
        }else{
            return(
                <>
                    <div className="container mt-3 mt-md-5">
                        <div className="row">
                            <div className="col-12 text-center d-flex justify-content-center mb-4 title_profile">
                                <h1>Sin informacion.</h1>
                            </div>
                        </div>
                    </div>
                </>
            )
        }
    }
}
export default Capitulos;