import React from 'react'
const Videos = ()=>{
    return(
        <>
            <div className="container">
                <div className="row">
                    <div className="col-12 title_section text-center">
                        <h2 className="mb-0 d-block">Lorem Ipsum</h2>
                    </div>
                    <div className="col-12 px-0">
                        <div className="video_container">
                            <iframe title="video de youtube" src="http://www.youtube.com/embed/NUsoVlDFqZg" 
                                    frameBorder="0"
                                    allow="autoplay,
                                    allowfullscreen"
                                    className="video_full">
                            </iframe>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
export default Videos;