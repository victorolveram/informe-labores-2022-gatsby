import React from 'react';
import { useEffect, useState } from 'react';
import axios from "axios";
import { webApiMagistradosDetalle } from "../../../routes/endpoints";
import { pathImages } from "../../../routes/endpoints";
import greca_perfil from '../../../assets/images/svg/greca_profile.svg';
import 'animate.css';

const Biografia = (info)=>{    
    const [magistrados,setMagistrados] = useState();
    useEffect(()=>{
        axios.post(webApiMagistradosDetalle,{id:info.info}).then((response) => {
            setMagistrados(response.data);
        }).catch(err => console.warn(err)); 
    },[info])
    if(!magistrados)
    {
        return(  
             <div>CARGANDO....</div>   
        )    
    }else{          
        if(magistrados.length){
            const  {imagen,nombre,paterno,materno,descripcion} = magistrados[0];        
            return(
                <>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12 greca_profile_container px-0 d-flex align-items-center">
                                <div className="greca_profile_img">
                                    <img src={greca_perfil} alt="" className="greca_img_profile animate__animated animate__fadeInRight"/>
                                </div>
                                <div className="greca_profile_img">
                                    <img src={greca_perfil} alt="" className="greca_img_profile animate__animated animate__fadeInLeft"/>
                                </div>
                                <img src={`${pathImages}${imagen}`} alt="" className="photo_profile_xl animate__animated"/>
                            </div>
                        </div>
                    </div>
                    <div className="container mt-3 mt-md-5">
                        <div className="row">
                            <div className="col-12 text-center d-flex justify-content-center mb-4 title_profile">
                                <h1>{nombre} <span>{paterno} {materno}</span></h1>
                            </div>
                            <div className="col-12">
                                <div className="paragraph_content" dangerouslySetInnerHTML={{__html: `${descripcion}` }}></div>
                            </div>
                        </div>
                    </div>
                </>
            );         
        }else{
            return(
                <>
                    <div className="container mt-3 mt-md-5">
                        <div className="row">
                            <div className="col-12 text-center d-flex justify-content-center mb-4 title_profile">
                                <h1>Sin informacion.</h1>
                            </div>
                        </div>
                    </div>
                </>
            )
        } 
    }
}
export default Biografia;