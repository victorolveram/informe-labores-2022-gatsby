import React from 'react'
import { base_url } from '../../constants'
import { Link } from "gatsby"
import { webApiMenu } from '../../routes/endpoints'
import { useEffect, useState } from 'react'
import axios from 'axios'
import { StaticImage } from "gatsby-plugin-image"
import { menuDTO } from './models/Layout.model'
const Header = () => {
    const [menu, setMenu] = useState<menuDTO[]>()
    useEffect(()=>{        
        import("bootstrap/dist/js/bootstrap");
        axios.get(webApiMenu).then((response) => {
            setMenu(response.data);
        }).catch(err => console.warn(err));         
    },[])
    return (
        <div className="container-fluid">
            <div className="row menu_container">
            
                <div className="col-sm-4 d-flex align-items-center justify-content-center justify-content-sm-start">
                <Link to={`/${base_url}/`} >
                    <StaticImage src={'../../assets/images/svg/tepjf_white_logo.svg'} alt={"TEPJF Logotipo"} className=" img-fluid ms-sm-5 mt-3 mt-sm-0"/>                 
                </Link>
                </div>
            
                <div className="col-sm-8 d-flex align-items-center justify-content-center justify-content-sm-end">

                    <div className="collapse" id="navbarToggleExternalContent">
                        <div className="p-4">
                            <ul className="navbar-nav navigation_main">                                
                                {/* {
                                    menu?.map((value,i)=> (
                                        <li className="text-white px-2" key={i}><Link to={value.url}>{value.nombre}</Link></li>
                                    ))
                                }  */}
                                <li className="text-white px-2"><Link to={`/`} >Inicio</Link></li>
                                <li className="text-white px-2"><Link to={`/magistrados/?q=1`}>Magistrados</Link></li>
                                <li className="text-white px-2"><Link to={`/capitulos/?q=1`}>Capítulo 1</Link></li>
                                <li className="text-white px-2"><Link to={`/capitulos/?q=2`}>Capítulo 2</Link></li>
                                <li className="text-white px-2"><Link to={`/capitulos/?q=3`}>Capítulo 3</Link></li>
                                <li className="text-white px-2"><Link to={`/capitulos/?q=4`}>Capítulo 4</Link></li>
                                <li className="text-white px-2"><Link to={`/capitulos/?q=5`}>Capítulo 5</Link></li>
                            </ul>                                   
                        </div>
                    </div>
                    <nav className="navbar navbar-dark">
                        <div className="container-fluid">
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="n_bar">
                            
                        </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    )};

export default Header;
