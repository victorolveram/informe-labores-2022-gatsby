
import React from 'react';
import { StaticImage } from "gatsby-plugin-image";

const Footer = () => {
    return (
        <div className="container-fluid text-white">
            <div className="row">
                <div className="col-12 footer_first">
                    <div className="container mt-5 mb-2">
                        <div className="row footer_text">
                            <div className="col-md-4 mb-3">
                                <div className="d-flex align-items-center justify-content-start">
                                    <h1 className="me-3">INFORME</h1>
                                    <div className="pleca_footer"></div>
                                </div>
                                <div className="mt-3">
                                    <p>Encuentra todo lo referente al Informe de Labores 2021/2022 y forma parte de la comunidad del Tribunal Electoral del Poder Judicial de la Federación. </p>    
                                </div>
                            </div>
                            <div className="col-md-4 mb-3">
                                <div className="d-flex align-items-center justify-content-start">
                                    <h1 className="me-3">CONTÁCTANOS</h1>
                                    <div className="pleca_footer"></div>
                                </div>
                                <div className="mt-3">
                                    <div className="row">
                                        <div className="col-4 col-sm-3">
                                            <h4>Dirección:</h4>
                                        </div>
                                        <div className="col-8 col-sm-9">
                                            <p>Av. Carlota Armero 5000, Coapa, Coyoacán, 04480.</p>
                                        </div>
                                    </div>
                                    <div className="row mt-1">
                                        <div className="col-4 col-sm-3">
                                            <h4>Teléfono:</h4>
                                        </div>
                                        <div className="col-8 col-sm-9">
                                            <p>55 57282300</p>
                                            <p>55 54845410</p>
                                        </div>
                                    </div>  
                                    <div className="row mt-1">
                                        <div className="col-4 col-sm-3">
                                            <h4>Email:</h4>
                                        </div>
                                        <div className="col-8 col-sm-9">
                                            <p>prensa@te.gob.mx</p>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                            <div className="col-md-4 mb-3">
                                <div className="d-flex align-items-center justify-content-start">
                                    <h1 className="me-3">REDES SOCIALES</h1>
                                    <div className="pleca_footer"></div>
                                </div>
                                <div className="mt-3">
                                    <p>Te invitamos a visitar nuestras cuentas de redes sociales y conocer más sobre el TEPJF.</p>    
                                </div>
                                <div className="div mt-2">
                                    <a href="#"><StaticImage src='../../assets/images/svg/facebook_icon.svg' className="icon_foot" alt="facebook icono"/></a>
                                    <a href="#"><StaticImage src='../../assets/images/svg/twitter_icon.svg' className="icon_foot" alt="twitter icono"/></a>
                                    <a href="#"><StaticImage src='../../assets/images/svg/instagram_icon.svg' className="icon_foot" alt="instagram icono"/></a>
                                    <a href="#"><StaticImage src='../../assets/images/svg/whatsapp_icon.svg' className="icon_foot" alt="whatsapp icono"/></a>
                                    <a href="#"><StaticImage src='../../assets/images/svg/youtube_icon.svg' className="icon_foot" alt="youtube icono"/></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-12 footer_second d-flex align-items-center justify-content-center text-center">
                    <p className="mb-0 p_footer_second my-2">Tribunal Electoral del Poder Judicial de la Federación. Todos los derechos reservados. 2022</p>
                </div>
            </div>
        </div>
    );
};

export default Footer;