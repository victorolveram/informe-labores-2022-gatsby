import axios from 'axios';
import { useEffect, useState } from 'react';


const usePeticion = ({verbo, endpoint, params}) => {
    const [data, setData] = useState([]);
    const [isFetching, setIsFetching] = useState(false);
    const [error, setError] = useState(null);
  
    useEffect(() => {
      consultarEndPoint();
    }, []);
  
    const consultarEndPoint = async () => {      
      try {
        setIsFetching(true);
        if(verbo === 'get'){
            axios.get(endpoint)
            .then(respuesta =>{
            setData(respuesta.data)
            })  
        }
      } catch (error) {
        console.log("el error es", error);
        setError(error)
      } finally{
        setIsFetching(false)
      }
    };
    return { data, isFetching, error };
};

export default usePeticion;

