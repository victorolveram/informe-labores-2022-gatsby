import { useStaticQuery, graphql } from "gatsby"

export const useServices = () => {
  const { allDatoCmsServicio, allDatoCmsRecomendacion } = useStaticQuery(graphql`
    {
      allDatoCmsServicio {
        nodes {
          titulo
          descripcion
          icono {
            gatsbyImageData
          }
        }
      }
      allDatoCmsRecomendacion {
        nodes {
          titulo
          descripcion
          distancia
          estadia
          costo
          slug
          imagen {
            gatsbyImageData
          }
        }
      }
    }
  `)

  const servicios = allDatoCmsServicio.nodes.map(item => {
    return {
      ...item,
      imagen: item.icono.gatsbyImageData,
    }
  })
  const recomendaciones = allDatoCmsRecomendacion.nodes.map(item => {
    return {
      ...item,
      imagen: item.imagen.gatsbyImageData,
    }
  })

  console.log("recomendaciones", recomendaciones)

  return { servicios, recomendaciones }
}
