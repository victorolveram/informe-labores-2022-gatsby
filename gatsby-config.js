module.exports = {
  flags: {
    DEV_SSR: true
  },
  pathPrefix: "/informe_2022_g",
  siteMetadata: {
    title: `Informe de Labores`,
    description: `Informe de Labores del TEPJF 2022`,
    author: `Victor Hugo Olvera Morales`,
    siteUrl: `http://localhost:8000/informe_2022_g/`,
  },
  plugins: [
    "gatsby-plugin-use-query-params",
    `gatsby-plugin-image`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        // This will impact how browsers show your PWA/website
        // https://css-tricks.com/meta-theme-color-and-trickery/
        // theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-source-datocms`,
      options: {
        apiToken: `1bfb231a85d25d8a8f226c1cf04276`,
      },
    },
  ],
}
